from .action import Action3
from mud.events import FillWithEvent

class FillWithAction(Action3):
    EVENT = FillWithEvent
    ACTION = "fill-with"
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_surroundings"  #Le liquide ne peut pas être logiquement dans l'inventaire.
