from .action import Action1
from mud.events import KneelEvent

class KneelAction(Action1):
    EVENT = KneelEvent
    ACTION = "kneel"
